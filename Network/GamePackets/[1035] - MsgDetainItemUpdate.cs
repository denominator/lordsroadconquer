﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LordsRoad.Game;

namespace LordsRoad.Network.GamePackets
{
    public class UpdateConfiscatedItem : Writer, Interfaces.IPacket
    {
        byte[] Buffer;

        public UpdateConfiscatedItem(bool Create)
        {
            if (Create)
            {
                Buffer = new byte[20 + 8];
                Writer.Write(20, 0, Buffer);
                Writer.Write(1035, 2, Buffer);
            }
        }
        
        public uint Page
        {
            get { return BitConverter.ToUInt32(Buffer, 4); }
            set { Write(value, 4, Buffer); }
        }

        public uint Update
        {
            get { return BitConverter.ToUInt32(Buffer, 8); }
            set { Write(value, 8, Buffer); }
        }

        public uint ItemUID
        {
            get { return BitConverter.ToUInt32(Buffer, 12); }
            set { Write(value, 12, Buffer); }
        }

        public uint DaysLeft
        {
            get { return (BitConverter.ToUInt32(Buffer, 16) + 7); }
            set { Write( 7 - value, 16, Buffer); }
        }

        public void Send(Client.GameClient client)
        {
            client.Send(Buffer);
        }
        public byte[] ToArray()
        {
            return Buffer;
        }
        public void Deserialize(byte[] buffer)
        {
            Buffer = buffer;
        }
    }
}
