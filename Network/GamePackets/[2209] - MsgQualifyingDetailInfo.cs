﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LordsRoad.Client;

namespace LordsRoad.Network.GamePackets
{
    public class ArenaStatistic : Writer, Interfaces.IPacket
    {
        public const uint NotSignedUp = 0,
                          WaitingForOpponent = 1,
                          WaitingInactive = 2;
        byte[] Buffer;
        public ArenaStatistic(bool Create)
        {
            Buffer = new byte[60];
            Writer.Write(52, 0, Buffer);
            Writer.Write(2209, 2, Buffer);
        }
        public uint Rank
        {
            get { return BitConverter.ToUInt32(Buffer, 4); }
            set { Write(value, 4, Buffer); }
        }

        public uint Status
        {
            get { return BitConverter.ToUInt32(Buffer, 12); }
            set { Write(value, 12, Buffer); }
        }

        public uint TotalWin
        {
            get { return BitConverter.ToUInt32(Buffer, 16); }
            set { Write(value, 16, Buffer); }
        }

        public uint TotalLose
        {
            get { return BitConverter.ToUInt32(Buffer, 20); }
            set { Write(value, 20, Buffer); }
        }

        public uint TodayWin
        {
            get { return BitConverter.ToUInt32(Buffer, 24); }
            set { Write(value, 24, Buffer); }
        }

        public uint TodayBattles
        {
            get { return BitConverter.ToUInt32(Buffer, 28); }
            set { Write(value, 28, Buffer); }
        }

        public uint HistoryHonor
        {
            get { return BitConverter.ToUInt32(Buffer, 32); }
            set { Write(value, 32, Buffer); }
        }

        public uint CurrentHonor
        {
            get { return BitConverter.ToUInt32(Buffer, 36); }
            set { Write(value, 36, Buffer); }
        }

        public uint ArenaPoints
        {
            get { return BitConverter.ToUInt32(Buffer, 40); }
            set { Write(value, 40, Buffer); }
        }
        public void Send(Client.GameClient client)
        {
            client.Send(ToArray());
        }

        public byte[] ToArray()
        {
            return Buffer;
        }

        public void Deserialize(byte[] buffer)
        {
            Buffer = buffer;
        }

        public byte Level
        {
            get;
            set;
        }
        public byte Class
        {
            get;
            set;
        }
        public uint Model
        {
            get;
            set;
        }
        public uint EntityID
        {
            get;
            set;
        }
        public uint LastSeasonRank
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }

        public DateTime LastArenaPointFill
        {
            get;
            set;
        }

        public Time32 AcceptBoxShow
        {
            get;
            set;
        }

        public bool AcceptBox
        {
            get;
            set;
        }

        public GameClient PlayWith
        {
            get;
            set;
        }

        public uint LastSeasonArenaPoints
        {
            get;
            set;
        }
        public uint LastSeasonWin
        {
            get;
            set;
        }
        public uint LastSeasonLose
        {
            get;
            set;
        }
        public uint YesterdayTotal
        {
            get
            {
                return LastSeasonWin + LastSeasonLose;
            }
        }
    }
}
