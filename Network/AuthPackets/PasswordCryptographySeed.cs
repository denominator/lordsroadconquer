﻿using System;
using System.Text;
namespace LordsRoad.Network.AuthPackets
{
    public class PasswordCryptographySeed : Interfaces.IPacket
    {
        byte[] Buffer;
        public PasswordCryptographySeed()
        {
            Buffer = new byte[8];
            Network.Writer.Write(8, 0, Buffer);
            Network.Writer.Write(1059, 2, Buffer);
        }
        public int Seed
        {
            get
            {
                return BitConverter.ToInt32(Buffer, 4);
            }
            set
            {
                Network.Writer.Write(value, 4, Buffer);
            }
        }

        public void Deserialize(byte[] buffer)
        {
            //no implementation
        }

        public byte[] ToArray()
        {
            return Buffer;
        }

        public void Send(Client.GameClient client)
        {
            client.Send(Buffer);
        }
    }
}
