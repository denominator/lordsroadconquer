﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using LordsRoad.Network;
using LordsRoad.Database;
using LordsRoad.Network.Sockets;
using LordsRoad.Network.GamePackets.Union;
using LordsRoad.Network.AuthPackets;
using LordsRoad.Game.ConquerStructures.Society;
using LordsRoad.Game;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using LordsRoad.Interfaces;
using System.Text;
using LordsRoad.Network.GamePackets;
using LordsRoad.Client;
namespace LordsRoad
{
    class Program
    {
        [DllImport("kernel32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetProcessWorkingSetSize(IntPtr process,
            UIntPtr minimumWorkingSetSize, UIntPtr maximumWorkingSetSize);
        public static Encoding Encoding = ASCIIEncoding.Default;
        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        public static DateTime LastRandomReset = DateTime.Now;
        private static Native.ConsoleEventHandler LordsRoadHandler;
        public static Client.GameClient[] Values = new Client.GameClient[0];
        public static void UpdateConsoleTitle()
        {
            if (Kernel.GamePool.Count > Program.MaxOn)
                Program.MaxOn = Kernel.GamePool.Count;
            if (Kernel.GamePool.Count != 0)
            {
                Console.Title = "Online Players: [ " + Kernel.GamePool.Count + " ] Max Online: [ " + Program.MaxOn + " ]";
            }
            else if (Kernel.GamePool.Count == 0)
            {
                Console.Title = "No Online Players Now!! But Max Online: [ " + Program.MaxOn + " ]";
            }
        }
        private static bool LordsRoadConsole_CloseEvent(CtrlType sig)
        {
            if (MessageBox.Show("Are you sure you want to close?", "LordsRoad", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Save();
                foreach (Client.GameClient client in Kernel.GamePool.Values)
                    client.Disconnect();
                GameServer.Disable();
                AuthServer.Disable();
                if (GuildWar.IsWar)
                    GuildWar.End();
                if (ClanWar.IsWar)
                    ClanWar.End();
                if (CaptureTheFlag.IsWar)
                    CaptureTheFlag.Close();
                Application.Exit();
                Environment.Exit(0);
                return false;
            }
            else return true;
        }
        public static void Save()
        {
            try
            {
                using (var conn = Database.DataHolder.MySqlConnection)
                {
                    conn.Open();
                    foreach (Client.GameClient client in Kernel.GamePool.Values)
                    {
                        client.Account.Save(client);
                        Database.EntityTable.SaveEntity(client, conn);
                        Database.DailyQuestTable.Save(client);
                        Database.SkillTable.SaveProficiencies(client, conn);
                        Database.ActivenessTable.Save(client);
                        Database.ChiTable.Save(client); 
                        Database.SkillTable.SaveSpells(client, conn);
                        Database.MailboxTable.Save(client);
                        Database.ArenaTable.SaveArenaStatistics(client.ArenaStatistic, client.CP, conn);
                        Database.TeamArenaTable.SaveArenaStatistics(client.TeamArenaStatistic, conn);
                    }
                }
                LordsRoad.Database.JiangHu.SaveJiangHu();
                AuctionBase.Save();
                Database.Flowers.SaveFlowers();
                Database.InnerPowerTable.Save();
                Database.EntityVariableTable.Save(0, Vars);
                 using (MySqlCommand cmd = new MySqlCommand(MySqlCommandType.SELECT))
                 {
                     cmd.Select("configuration");
                     using (MySqlReader r = new MySqlReader(cmd))
                     {
                         if (r.Read())
                         {
                             new Database.MySqlCommand(Database.MySqlCommandType.UPDATE).Update("configuration").Set("ServerKingdom", Kernel.ServerKingdom).Set("ItemUID", Network.GamePackets.ConquerItem.ItemUID.Now).Set("GuildID", Game.ConquerStructures.Society.Guild.GuildCounter.Now).Set("UnionID", Union.UnionCounter.Now).Execute();
                             if (r.ReadByte("LastDailySignReset") != DateTime.Now.Month) MsgSignIn.Reset();
                         }
                     }
                 }
                using (var cmd = new MySqlCommand(MySqlCommandType.UPDATE).Update("configuration"))
                    cmd.Set("LastDailySignReset", DateTime.Now.Month).Execute();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        static void GameServer_OnClientReceive(byte[] buffer, int length, ClientWrapper obj)
        {
            if (obj.Connector == null)
            {
                obj.Disconnect();
                return;
            }

            Client.GameClient Client = obj.Connector as Client.GameClient;

            if (Client.Exchange)
            {
                Client.Exchange = false;
                Client.Action = 1;
                var crypto = new Network.Cryptography.GameCryptography(System.Text.Encoding.Default.GetBytes(Constants.GameCryptographyKey));
                byte[] otherData = new byte[length];
                Array.Copy(buffer, otherData, length);
                crypto.Decrypt(otherData, length);

                bool extra = false;
                int pos = 0;
                if (BitConverter.ToInt32(otherData, length - 140) == 128)//no extra packet
                {
                    pos = length - 140;
                    Client.Cryptography.Decrypt(buffer, length);
                }
                else if (BitConverter.ToInt32(otherData, length - 176) == 128)//extra packet
                {
                    pos = length - 176;
                    extra = true;
                    Client.Cryptography.Decrypt(buffer, length - 36);
                }
                int len = BitConverter.ToInt32(buffer, pos); pos += 4;
                if (len != 128)
                {
                    Client.Disconnect();
                    return;
                }
                byte[] pubKey = new byte[128];
                for (int x = 0; x < len; x++, pos++) pubKey[x] = buffer[pos];

                string PubKey = System.Text.Encoding.Default.GetString(pubKey);
                Client.Cryptography = Client.DHKeyExchange.HandleClientKeyPacket(PubKey, Client.Cryptography);

                if (extra)
                {
                    byte[] data = new byte[36];
                    Buffer.BlockCopy(buffer, length - 36, data, 0, 36);
                    processData(data, 36, Client);
                }
            }
            else
            {
                processData(buffer, length, Client);
            }
        }
        private static void processData(byte[] buffer, int length, Client.GameClient Client)
        {
            Client.Cryptography.Decrypt(buffer, length);
            Client.Queue.Enqueue(buffer, length);
            if (Client.Queue.CurrentLength > 1224)
            {
                Console.WriteLine("[Disconnect]Reason:The packet size is too big. " + Client.Queue.CurrentLength);
                Client.Disconnect();
                return;
            }
            while (Client.Queue.CanDequeue())
            {
                byte[] data = Client.Queue.Dequeue();
                Network.PacketHandler.HandlePacket(data, Client);
            }
        }
        static void GameServer_OnClientConnect(ClientWrapper obj)
        {
            Client.GameClient client = new Client.GameClient(obj);
            client.Send(client.DHKeyExchange.CreateServerKeyPacket());
            obj.Connector = client;
        }
        static void GameServer_OnClientDisconnect(ClientWrapper obj)
        {
            if (obj.Connector != null)
                (obj.Connector as Client.GameClient).Disconnect();
            else
                obj.Disconnect();
        }
        static void AuthServer_OnClientReceive(byte[] buffer, int length, ClientWrapper arg3)
        {
            var player = arg3.Connector as Client.AuthClient;
            AuthClient authClient = arg3.Connector as AuthClient;
            player.Cryptographer.Decrypt(buffer, length);
            player.Queue.Enqueue(buffer, length);
            while (player.Queue.CanDequeue())
            {
                byte[] packet = player.Queue.Dequeue();

                ushort len = BitConverter.ToUInt16(packet, 0);
                ushort id = BitConverter.ToUInt16(packet, 2);

                if (len == 312)
                {
                    player.Info = new Authentication();
                    player.Info.Deserialize(packet);
                    player.Account = new AccountTable(player.Info.Username);
                    if (!BruteForceProtection.AcceptJoin(arg3.IP))
                    {
                        Console.WriteLine(string.Concat(new string[] { "Client > ", player.Info.Username, "was blocked address", arg3.IP, "!" }));
                        arg3.Disconnect();
                        break;
                    }
                    Forward Fw = new Forward();
                    if (player.Account.Password == player.Info.Password && player.Account.exists)
                    {
                        Fw.Type = Forward.ForwardType.Ready;
                    }
                    else
                    {
                        BruteForceProtection.ClientRegistred(arg3.IP);
                        Fw.Type = Forward.ForwardType.InvalidInfo;
                    }
                    if (IPBan.IsBanned(arg3.IP))
                    {
                        Fw.Type = Forward.ForwardType.Banned;
                        player.Send(Fw);
                        return;
                    }
                    if (Fw.Type == Network.AuthPackets.Forward.ForwardType.Ready)
                    {
                        Fw.Identifier = player.Account.GenerateKey();
                        Kernel.AwaitingPool[Fw.Identifier] = player.Account;
                        Fw.IP = GameIP;
                        Fw.Port = GamePort;
                    }
                    player.Send(Fw);
                }
            }
        }
        public static void LoadServer(bool KnowConfig)
        {
            RandomSeed = Convert.ToInt32(DateTime.Now.Ticks.ToString().Remove(DateTime.Now.Ticks.ToString().Length / 2));
            Console.Title = "Loading...";
            Console.WriteLine("Loading...");
            Kernel.Random = new FastRandom(RandomSeed);
            #region Configuration
            if (!KnowConfig)
            {
                DatabaseName = "cq6300";
                DatabasePass = "password";
            }
            Database.DataHolder.CreateConnection(DatabaseName, DatabasePass);
            Database.EntityTable.EntityUID = new Counter(0);
            new MySqlCommand(MySqlCommandType.UPDATE).Update("entities").Set("Online", 0).Execute();
            using (MySqlCommand cmd = new MySqlCommand(MySqlCommandType.SELECT))
            {
                cmd.Select("configuration");
                using (MySqlReader r = new MySqlReader(cmd))
                {
                    if (r.Read())
                    {
                        if (!KnowConfig)
                        {
                            GameIP = r.ReadString("ServerIP");
                            GamePort = 5816;
                            AuthPort = r.ReadUInt16("ServerPort");
                        }
                        Database.EntityTable.EntityUID = new Counter(r.ReadUInt32("EntityID"));
                        if (Database.EntityTable.EntityUID.Now == 0)
                            Database.EntityTable.EntityUID.Now = 1;
                        Union.UnionCounter = new Counter(r.ReadUInt32("UnionID"));
                        Kernel.ServerKingdom = (r.ReadUInt32("ServerKingdom"));
                        if (r.ReadByte("LastDailySignReset") != DateTime.Now.Month) MsgSignIn.Reset();
                        Game.ConquerStructures.Society.Guild.GuildCounter = new LordsRoad.Counter(r.ReadUInt32("GuildID"));
                        Network.GamePackets.ConquerItem.ItemUID = new LordsRoad.Counter(r.ReadUInt32("ItemUID"));
                        Constants.ExtraExperienceRate = r.ReadUInt32("ExperienceRate");
                        Constants.ExtraSpellRate = r.ReadUInt32("ProficiencyExperienceRate");
                        Constants.ExtraProficiencyRate = r.ReadUInt32("SpellExperienceRate");
                        Constants.MoneyDropRate = r.ReadUInt32("MoneyDropRate");
                        Constants.ConquerPointsDropRate = r.ReadUInt32("ConquerPointsDropRate");
                        Constants.ItemDropRate = r.ReadUInt32("ItemDropRate");
                        Constants.ItemDropQualityRates = r.ReadString("ItemDropQualityString").Split('~');
                        Database.EntityVariableTable.Load(0, out Vars);
                    }
                }
            }
            using (var cmd = new MySqlCommand(MySqlCommandType.UPDATE).Update("configuration"))
                cmd.Set("LastDailySignReset", DateTime.Now.Month).Execute();
            #endregion
            Database.JiangHu.LoadStatus();
            Database.JiangHu.LoadJiangHu();
            Way2Heroes.Load();
            QuestInfo.Load();
            AuctionBase.Load();
            Database.DataHolder.ReadStats();
            LordsRoad.Soul.SoulProtection.Load();
            Database.PerfectionTable.Load();
            Database.LotteryTable.Load();
            Database.ConquerItemTable.ClearNulledItems();
            Database.ConquerItemInformation.Load();
            Database.MonsterInformation.Load();
            Database.IPBan.Load();
            Database.SpellTable.Load();
            Database.ShopFile.Load();
            Database.HonorShop.Load();
            Database.RacePointShop.Load();
            Database.ChampionShop.Load();
            Database.EShopFile.Load();
            Database.EShopV2File.Load();
            Database.MapsTable.Load();
            Database.Flowers.LoadFlowers();
            Database.NobilityTable.Load();
            Database.ArenaTable.Load();
            Database.TeamArenaTable.Load();
            Database.GuildTable.Load();
            Database.ChiTable.LoadAllChi();
            Refinery.LoadItems();
            StorageManager.Load();
            Database.StorageItem.Load();
            UnionTable.Load();
            World = new World();
            World.Init();
            Database.InnerPowerTable.LoadDBInformation();
            Database.InnerPowerTable.Load();
            Map.CreateTimerFactories();
            Database.SignInTable.Load();
            Database.DMaps.Load();
            Game.Screen.CreateTimerFactories();
            World.CreateTournaments();
            Game.GuildWar.Initiate();
            Game.ClanWar.Initiate();
            Game.Tournaments.SkillTournament.LoadSkillTop8();
            Game.Tournaments.TeamTournament.LoadTeamTop8();
            Clan.LoadClans();
            Booths.Load();
            Database.FloorItemTable.Load();
            Database.ReincarnationTable.Load();
            new MsgUserAbilityScore().GetRankingList();
            new MsgEquipRefineRank().UpdateRanking();
            BruteForceProtection.CreatePoll();
            {
                Client.GameClient gc = new Client.GameClient(new ClientWrapper());
                gc.Account = new AccountTable("NONE");
                gc.Socket.Alive = false;
                gc.Entity = new Entity(EntityFlag.Player, false) { Name = "NONE" };
                Npcs.GetDialog(new NpcRequest(), gc, true);
            }
            #region OpenSocket
            Network.Cryptography.AuthCryptography.PrepareAuthCryptography();
            AuthServer = new ServerSocket();
            AuthServer.OnClientConnect += AuthServer_OnClientConnect;
            AuthServer.OnClientReceive += AuthServer_OnClientReceive;
            AuthServer.OnClientDisconnect += AuthServer_OnClientDisconnect;
            AuthServer.Enable(AuthPort, "0.0.0.0");
            GameServer = new ServerSocket();
            GameServer.OnClientConnect += GameServer_OnClientConnect;
            GameServer.OnClientReceive += GameServer_OnClientReceive;
            GameServer.OnClientDisconnect += GameServer_OnClientDisconnect;
            GameServer.Enable(GamePort, "0.0.0.0");
            #endregion
            Console.Clear();
            Console.Title = "Server Running Normally!";
            Console.WriteLine("Server Loaded Successfully!!");
            LordsRoadHandler += LordsRoadConsole_CloseEvent;
            Native.SetConsoleCtrlHandler(LordsRoadHandler, true);
        }
        static void AuthServer_OnClientDisconnect(ClientWrapper obj)
        {
            obj.Disconnect();
        }
        static void AuthServer_OnClientConnect(ClientWrapper obj)
        {
            Client.AuthClient authState;
            obj.Connector = (authState = new Client.AuthClient(obj));
            authState.Cryptographer = new Network.Cryptography.AuthCryptography();
            Network.AuthPackets.PasswordCryptographySeed pcs = new PasswordCryptographySeed();
            pcs.Seed = Kernel.Random.Next();
            authState.PasswordSeed = pcs.Seed;
            authState.Send(pcs);
        }
        public static ServerSocket AuthServer;
        public static ServerSocket GameServer;
        public static string GameIP;
        public static ushort GamePort;
        public static string DatabaseName;
        public static string DatabasePass;
        public static ushort AuthPort;
        public static World World;
        public static VariableVault Vars;
        public static int MaxOn = 0;
        public static int RandomSeed = 0;
        static void Main()
        {
            LoadServer(false);
        }
    }
}