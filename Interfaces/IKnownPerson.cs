﻿using System;

namespace LordsRoad.Interfaces
{
    public interface IKnownPerson
    {
        uint ID { get; set; }
        string Name { get; set; }
        bool IsOnline { get; }
        Client.GameClient Client { get; }
    }
}
