﻿using System;

namespace LordsRoad.Interfaces
{
    public interface IPacket
    {
        byte[] ToArray();
        void Deserialize(byte[] buffer);
        void Send(Client.GameClient client);
    }
}
